namespace jBloger.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBlogUrl : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Author", c => c.String());
            AlterColumn("dbo.Comments", "Content", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            AlterColumn("dbo.Posts", "Summary", c => c.String());
            AlterColumn("dbo.Posts", "Content", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Content", c => c.Long(nullable: false));
            AlterColumn("dbo.Posts", "Summary", c => c.Long(nullable: false));
            AlterColumn("dbo.Posts", "Title", c => c.Long(nullable: false));
            AlterColumn("dbo.Comments", "Content", c => c.Long(nullable: false));
            AlterColumn("dbo.Comments", "Author", c => c.Long(nullable: false));
        }
    }
}
