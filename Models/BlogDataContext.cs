﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace jBloger.Models
{
    
    public class BlogDataContext : DbContext
    {
        public BlogDataContext()
            : base("name=JonyModelConSet")
    {
        
    }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
     
}