﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jBloger.Models;
using System.Data.Entity;

namespace jBloger.Controllers
{
    public class BlogController : Controller
    {
        JonyModelConSet _db = new JonyModelConSet();
        // GET: Blog
        public ActionResult Indexx()
        {
            var posts = _db.Posts.ToList();
            
            return View(posts);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                
                post.Created = DateTime.Now.ToString();
                _db.Posts.Add(post);
                _db.SaveChanges();
                return RedirectToAction("Indexx");
            }
            return View(post);
        }

        public ActionResult Post(long? id) 
        {
            var post = _db.Posts.Find(id);
            return View(post);
        }
        [HttpPost]
        public ActionResult Post(long? id,Comment newComment)
        {
            if (ModelState.IsValid && id != null) 
            {
               newComment.Id == _db.Comments.Max(c => c.Id+1)
                    newComment.Post = (int)id;
                    newComment.Created = DateTime.Now.ToString();
                    _db.Comments.Add(newComment);
                    _db.SaveChanges();
                    return RedirectToAction("Post", new { Id = id });
                
            }
            return Post(id);
        }
        public ActionResult Edit(long? id)
        {
             return View(_db.Posts.Find(id));
        }
        [HttpPost]
        public ActionResult Edit(long? id, jBloger.Models.Post post)
        {
            if (ModelState.IsValid)
            {
                post.Created = DateTime.Now.ToString();
                _db.Entry(post).State = EntityState.Modified;
                 
                _db.SaveChanges();
                return RedirectToAction("Indexx");
            }
            return View(post);
        }

        public ActionResult Delete(long? id)
        {

            return View(_db.Posts.Find(id));

        }
        [HttpPost,ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(long? id, Post post)
        {

            Post posts = _db.Posts.Find(id);
            
            _db.Posts.Remove(posts);
            
            _db.SaveChanges();
            return RedirectToAction("Indexx");
        }
        
        public ActionResult DeleteComments(long? id)
        {

            return View(_db.Comments.Find(id));

        }
        [HttpPost, ActionName("DeleteComments")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteComments(long? id, Comment comment)
        {

            Comment comments = _db.Comments.Find(id);
            _db.Comments.Remove(comments);
            
            _db.SaveChanges();
            return RedirectToAction("Post");
        }
        public ActionResult EditComments(long? id)
        {
            return View(_db.Comments.Find(id));
        }
        [HttpPost]
        public ActionResult EditComments(long? id, jBloger.Models.Comment comment)
        {
            if (ModelState.IsValid)
            {
                comment.Created = DateTime.Now.ToString();
                _db.Entry(comment).State = EntityState.Modified;
              // comment.Id = _db.Comments.Max(c => c.Id) + 1;
             // comment.Post = (int)id;
                
                _db.SaveChanges();
                return RedirectToAction("Post");
            }
            return View(comment);
        }
        public PartialViewResult AjaxSearch(string q)
        {
            var albums = _db.Posts.Where(a => a.Title.Contains(q)).Take(10);
            return this.PartialView(albums);
        }
            

    }
}