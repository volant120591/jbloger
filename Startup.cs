﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jBloger.Startup))]
namespace jBloger
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
